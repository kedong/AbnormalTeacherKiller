package hangxingliu.android.atkcontroller;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public final class UDPSender {
	
	private final static void _Send(byte[] data,int length,String toip,int port) throws IOException{
		InetAddress address = InetAddress.getByName(toip);
		DatagramPacket dp = new DatagramPacket(data,0,length,address,port);
		DatagramSocket ds = new DatagramSocket();
		ds.send(dp);
		ds.close();
	}
	
	public final static Exception Send(byte[] data,int length,String toip,int port){
		try {
			_Send(data, length, toip, port);
		} catch (Exception e) {
			return e;
		}
		return null;
	}
}
