package hangxingliu.android.atkcontroller;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class VDrawable extends Drawable {

	private Bitmap bmp;
	private Rect src,dst;
	private int w, h;
	public VDrawable(Resources res,int w,int h,int id) {
		bmp = BitmapFactory.decodeResource(res, id);
		src = new Rect(0, 0, bmp.getWidth(),bmp.getHeight());
		dst = new Rect(0,0,w,h);
		this.w = w;
		this.h = h;
	}
	
	@Override
	public int getIntrinsicWidth() {
		return w;
	}
	
	@Override
	public int getIntrinsicHeight() {
		return h;
	}
	
	@Override
	public void draw(Canvas canvas) {
		canvas.drawBitmap(bmp, src, dst, null);
	}

	@Override
	public int getOpacity() {
		return 0;
	}

	@Override
	public void setAlpha(int alpha) {

	}

	@Override
	public void setColorFilter(ColorFilter cf) {

	}

}
