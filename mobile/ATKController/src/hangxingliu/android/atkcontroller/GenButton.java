package hangxingliu.android.atkcontroller;

import android.content.Context;
import android.widget.Button;

public final class GenButton {

	public static Button gen(Context c,String text,int tag){
		Button btn = new Button(c);
		btn.setText(text);
		btn.setTextSize(30.0f);
		btn.setTag(tag);
		return btn;
	}
}
