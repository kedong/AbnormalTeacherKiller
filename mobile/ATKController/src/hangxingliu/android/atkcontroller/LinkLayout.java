package hangxingliu.android.atkcontroller;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("ViewConstructor")
public class LinkLayout extends LinearLayout implements android.view.View.OnClickListener {

	private TextView tvAddress;
	private EditText etAddress;
	private Button btnLink;
	private TextView tvResult;
	
	private Activity context;
	
	private Thread threadCall = null;
	private Thread threadResult = null;
	private Thread threadTime = null;
	
	private DatagramSocket ds = null;
	
	public LinkLayout(Activity context) {
		super(context);
		this.context = context;
		
		initView();
	}

	private void initView() {
		this.setOrientation(VERTICAL);
		
		tvAddress = new TextView(context);
		tvAddress.setText("目标地址:");
		tvAddress.setTextSize(20.0f);
		etAddress = new EditText(context);
		etAddress.setText("192.168.");
		etAddress.setSingleLine(true);
		
		btnLink = new Button(context);
		btnLink.setText("连接");
		btnLink.setOnClickListener(this);
		
		tvResult = new TextView(context);
		tvResult.setText("未连接");
		tvResult.setTextColor(Color.YELLOW);
		tvResult.setTextSize(20.0f);
		
		this.addView(tvAddress);
		this.addView(etAddress);
		this.addView(btnLink);
		this.addView(tvResult);
	}

	@Override
	public void onClick(View view) {
		if(threadCall != null)return ;
		view.setEnabled(false);
		final String toip = etAddress.getText().toString();
		threadResult =new Thread(new Runnable() {
			@Override
			public void run() {
				
				try {
					ds = new DatagramSocket(9701);
					DatagramPacket dp = new DatagramPacket(new byte[16], 16);
					ds.receive(dp);
					if(dp != null){
						byte[] data = dp.getData();
						if(dp.getLength() > 0){
							int i = (int)(data[0]);
							if(((i + 256) % 256) == 88){
								LinkInfo.address = dp.getAddress();
								try{threadTime.interrupt();}catch(Exception e){}
								threadTime = null;
								threadCall = null;
								threadResult = null;
								context.runOnUiThread(new Runnable() {
									@Override
									public void run() {
										tvResult.setText("已连接到:".concat(LinkInfo.address.getHostAddress()));
										tvResult.setTextColor(Color.GREEN);
										btnLink.setEnabled(true);
									}
								});
							}
						}
					}
					
				} catch (Exception e) {
					final Exception e2 = e;
					context.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							new AlertDialog.Builder(context).setTitle("接收连接应答出错!")
							.setMessage(e2.getClass().getCanonicalName()).setPositiveButton("确定", null)
							.create().show();
						}
					});
				}
				if(ds!=null)
					ds.close();
				threadResult = null;
			}
		},"等待回应线程");
		threadResult.start();
		threadTime = new Thread(new Runnable() {			
			@Override
			public void run() {
				try {
					Thread.sleep(10 * 1000);
					
				} catch (InterruptedException e) {
					return ;
				}
				if(threadResult != null)
					try{ds.close();threadResult.interrupt();}catch(Exception e){}
				threadCall = null;
				threadTime = null;
				context.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						btnLink.setEnabled(true);
						new AlertDialog.Builder(context).setTitle("连接超时!")
						.setMessage(etAddress.getText().toString().concat("无回应!")).setPositiveButton("确定", null)
						.create().show();
					}
				});
			}
		});
		threadTime.start();
		threadCall = new Thread(new Runnable() {
			
			@Override
			public void run() {
				final Exception e = UDPSender.Send(Cmd.Hello, Cmd.Hello.length,
						toip, 9711);
				if(e != null){
					if(threadTime != null)
						try{threadTime.interrupt();}catch(Exception e2){}
					if(threadResult != null)
						try{threadResult.interrupt();}catch(Exception e3){}
					threadTime = null;
					threadResult = null;
					threadCall = null;
					context.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							new AlertDialog.Builder(context).setTitle("连接出错!")
								.setMessage(e.getMessage()).setPositiveButton("确定", null)
								.create().show();
						}
					});
				}
			}
		}, "在线吗呼叫线程");
		threadCall.start();
		
	}
}
