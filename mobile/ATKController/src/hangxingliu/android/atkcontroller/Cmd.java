package hangxingliu.android.atkcontroller;

public class Cmd {

	public final static byte[] Hello = new byte[]{
		(byte)(0),
		(byte)(0),(byte)(0),(byte)(0),(byte)(0),(byte)(0),(byte)(0)
	};
	
	public final static byte[] Mouse = new byte[]{
		(byte)(1),
		(byte)(0),(byte)(0),(byte)(0),(byte)(0),(byte)(0),(byte)(0),
		(byte)(0)
	};
	
	public static byte[] genMouse(byte action){
		byte[] res = new byte[8];
		System.arraycopy(Mouse, 0, res, 0, 8);
		res[7] = action;
		return res;
	}
}
