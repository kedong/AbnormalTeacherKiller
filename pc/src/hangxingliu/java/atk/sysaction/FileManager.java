package hangxingliu.java.atk.sysaction;

import hangxingliu.java.atk.Const;
import hangxingliu.java.atk.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public final class FileManager {

	private static int threadCount = 0;
	
	public static void fileList(String path,InetAddress address){
		path = path.trim();
		Log.addLog("UDP请求文件列表:".concat(path));
		File[] list = null;
		String res = "";
		if(path.equals("/") || path.equals("\\")){
			list = File.listRoots();
			res = "Root";
			for(File f1 : list){
				res.concat("\nabs:").concat(f1.getAbsolutePath());
			}
		}else{
			File f = new File(path);
			if(f.exists() && f.canRead()){
				list = f.listFiles();
			}
			res = f.getAbsolutePath();
			if(list != null){
				for(File f2 : list){
					if(f2.isFile())
						res.concat("\nfile:").concat(f2.getName());
					else if(f2.isDirectory())
						res.concat("\n").concat(f2.getName());
				}
			}
		}
		try {
			Socket s = new Socket(address,Const.PORT_TCP_CLIENT_FILE_LIST);
			s.getOutputStream().write(res.getBytes());
			s.close();
		} catch (IOException e) {
			Log.addLog("返回文件列表出错:\n".concat(e.getMessage()));
			return ;
		}
		Log.addLog("返回文件列表成功!");
		
	}

	private static void _downloadFile(String file,InetAddress address){
		Log.addLog("UDP请求下载文件:".concat(file));
		FileInputStream fis = null;
		OutputStream os = null;
		int bufLength = 1<<16;
		byte[] buf = new byte[bufLength];
		int length;
		
		try {
			fis = new FileInputStream(file);
			Socket s = new Socket(address, Const.PORT_TCP_CLIENT_FILE_DOWNLOAD);
			os = s.getOutputStream();
			while((length = fis.read(buf)) > 0){
				os.write(buf, 0, length);
			}
			os.close();
			s.close();
			fis.close();
		} catch (FileNotFoundException e) {
			Log.addLog("下载文件失败,文件不存在!\n".concat(e.getMessage()));
			return ;
		} catch (IOException e) {
			Log.addLog("下载文件失败,IO错误!\n".concat(e.getMessage()));
			return ;
		}
		Log.addLog("下载文件成功!");
	}

	public static void downloadFile(final String file,final InetAddress address){
		if(threadCount > 100)
			threadCount = 0;
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				_downloadFile(file,address);
			}
		},"发送文件线程-".concat(Integer.toString(threadCount++))).start();
	}
}
