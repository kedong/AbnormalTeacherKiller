package hangxingliu.java.atk.sysaction;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class VLRobot extends Robot {

	private int version = 1;

	/**
	 * 字符的KeyEvent码
	 */
	private int charcode[];

	/**
	 * 拖动鼠标(默认左键)
	 * 
	 * @param to_x
	 *            拖动到X
	 * @param to_y
	 *            拖动到Y
	 */
	public void VDragMouse(int to_x, int to_y) {
		this.VPressMouse('L');
		this.mouseMove(to_x, to_y);
		this.VReleaseMouse('L');
	}
	
	/**
	 * 拖动鼠标()
	 * 
	 * @param to_x
	 *            拖动到X
	 * @param to_y
	 *            拖动到Y
	 * @param mousecode
	 *            鼠标按键代码<br/>
	 *            R:右键<br/>
	 *            M:中键<br/>
	 *            L:左键
	 */
	public void VDragMouse(int to_x, int to_y,char mousecode) {
		this.VPressMouse(mousecode);
		this.mouseMove(to_x, to_y);
		this.VReleaseMouse(mousecode);
	}

	/**
	 * 截取屏幕
	 * 
	 * @return 屏幕图像
	 */
	public BufferedImage VCaptureScreen() {
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		return this.createScreenCapture(new Rectangle(0, 0, scrSize.width,
				scrSize.height));
	}

	/**
	 * 截取屏幕
	 * 
	 * @param x
	 *            左上角X坐标
	 * @param y
	 *            左上角Y坐标
	 * @param w
	 *            截取宽度
	 * @param h
	 *            截取高度
	 * @return 屏幕图像
	 */
	public BufferedImage VCaptureScreen(int x, int y, int w, int h) {
		return this.createScreenCapture(new Rectangle(x, y, w, h));
	}

	/**
	 * 点击一下鼠标(默认位置)
	 * 
	 * @param mousecode
	 *            鼠标按键代码<br/>
	 *            R:右键<br/>
	 *            M:中键<br/>
	 *            L:左键
	 */
	public void VClickMouse(char mousecode) {
		int v = InputEvent.BUTTON3_MASK;
		if (mousecode == 'M' || mousecode == 'm')
			v = InputEvent.BUTTON2_MASK;
		else if (mousecode == 'L' || mousecode == 'l')
			v = InputEvent.BUTTON1_MASK;
		this.mousePress(v);
		this.mouseRelease(v);
	}

	/**
	 * 点击一下鼠标(指定坐标)
	 * 
	 * @param x
	 *            X坐标
	 * @param y
	 *            Y坐标
	 * @param mousecode
	 *            鼠标按键代码<br/>
	 *            R:右键<br/>
	 *            M:中键<br/>
	 *            L:左键
	 */
	public void VClickMouse(int x, int y, char mousecode) {
		int v = InputEvent.BUTTON3_MASK;
		if (mousecode == 'M' || mousecode == 'm')
			v = InputEvent.BUTTON2_MASK;
		else if (mousecode == 'L' || mousecode == 'l')
			v = InputEvent.BUTTON1_MASK;
		this.mouseMove(x, y);
		this.mousePress(v);
		this.mouseRelease(v);
	}

	/**
	 * 按下鼠标(指定坐标)
	 * 
	 * @param mousecode 
	 * 			    鼠标按键代码<br/>
	 *            R:右键<br/>
	 *            M:中键<br/>
	 *            L:左键
	 */
	public void VPressMouse(int mousecode) {
		int v = InputEvent.BUTTON3_MASK;
		if (mousecode == 'M' || mousecode == 'm')
			v = InputEvent.BUTTON2_MASK;
		else if (mousecode == 'L' || mousecode == 'l')
			v = InputEvent.BUTTON1_MASK;
		this.mousePress(v);
	}
	
	/**
	 * 释放鼠标(指定坐标)
	 * 
	 * @param mousecode 
	 * 			    鼠标按键代码<br/>
	 *            R:右键<br/>
	 *            M:中键<br/>
	 *            L:左键
	 */
	public void VReleaseMouse(int mousecode) {
		int v = InputEvent.BUTTON3_MASK;
		if (mousecode == 'M' || mousecode == 'm')
			v = InputEvent.BUTTON2_MASK;
		else if (mousecode == 'L' || mousecode == 'l')
			v = InputEvent.BUTTON1_MASK;
		this.mouseRelease(v);
	}

	/**
	 * 按下一个键，然后释放
	 * 
	 * @param keycode
	 *            键的KeyCode
	 */
	public void VKnockKey(int keycode) {
		this.keyPress(keycode);
		this.keyRelease(keycode);
	}

	/**
	 * 按下多个键，然后释放
	 * 
	 * @param keycodes
	 *            键的KeyCode的数组
	 */
	public void VKnockKey(int[] keycodes) {
		for (int kc : keycodes)
			this.keyPress(kc);
		for (int kc : keycodes)
			this.keyRelease(kc);
	}

	/**
	 * 按入一个字符串
	 * 
	 * @param data
	 *            字符串(<B>注意：只允许是键盘上可以敲出来的字符组成的</B>)
	 * @return 返回状态码<br/>
	 *         0:成功<br/>
	 *         -1:错误<br/>
	 *         1:字符串没有内容
	 */
	public int VInputString(String data) {
		if (data == null)
			return 1;
		if (data.length() == 0)
			return 1;
		int size = data.length();
		for (int i = 0; i < size; i++) {
			char t = data.charAt(i);
			int cc = charcode[t];
			if (t < 0 || t >= 300)
				return -1;
			if (cc / 1000 == 1) {
				this.VKnockKey(new int[] { 16, cc % 1000 });
			} else {
				this.VKnockKey(cc);
			}
		}
		return 0;
	}

	/**
	 * 初始化函数
	 */
	private void init() {
		init_arr();
	}

	/**
	 * 初始化Charcode数组
	 */
	private void init_arr() {
		charcode = new int[300];
		Arrays.fill(charcode, 32);
		charcode['~'] = 192;
		charcode['`'] = 1192;
		charcode['!'] = 1049;
		charcode['@'] = 1050;
		charcode['#'] = 1051;
		charcode['$'] = 1052;
		charcode['%'] = 1053;
		charcode['^'] = 1054;
		charcode['&'] = 1055;
		charcode['*'] = 1056;
		charcode['('] = 1057;
		charcode[')'] = 1048;
		charcode['-'] = 45;
		charcode['_'] = 1045;
		charcode['='] = 61;
		charcode['+'] = 1061;
		charcode['['] = 91;
		charcode['{'] = 1091;
		charcode[']'] = 93;
		charcode['}'] = 1093;
		charcode['\\'] = 42;
		charcode['|'] = 1042;
		charcode[';'] = 59;
		charcode[':'] = 1059;
		charcode['\''] = 222;
		charcode['\"'] = 1222;
		charcode['\n'] = 10;
		charcode[','] = 44;
		charcode['<'] = 1044;
		charcode['.'] = 46;
		charcode['>'] = 1046;
		charcode['/'] = 47;
		charcode['?'] = 1047;
		charcode[' '] = 32;
		for (int i = 65; i <= 90; i++) {
			charcode[i + 32] = i;
			charcode[i] = 1000 + i;
		}
		for (int i = 48; i <= 57; i++)
			charcode[i] = i;
	}

	// -------------------Version-----------------------
	public int getVersion() {
		return this.version;
	}

	// -------------------------------------------------
	// -----------------以下为继承的方法-----------------
	// -------------------------------------------------

	public VLRobot() throws AWTException {
		super();
		init();
	}

	public VLRobot(GraphicsDevice arg0) throws AWTException {
		super(arg0);
		init();
	}

	@Override
	public synchronized void keyPress(int arg0) {
		// TODO 自动生成的方法存根
		super.keyPress(arg0);
	}

	@Override
	public synchronized void keyRelease(int arg0) {
		// TODO 自动生成的方法存根
		super.keyRelease(arg0);
	}

	@Override
	public synchronized void mouseMove(int arg0, int arg1) {
		// TODO 自动生成的方法存根
		super.mouseMove(arg0, arg1);
	}

	@Override
	public synchronized void mousePress(int arg0) {
		// TODO 自动生成的方法存根
		super.mousePress(arg0);
	}

	@Override
	public synchronized void mouseRelease(int arg0) {
		// TODO 自动生成的方法存根
		super.mouseRelease(arg0);
	}

}
