package hangxingliu.java.atk;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;


public class MainServer {

	private Thread threadListener;
	
	private boolean continueThread = true;
	
	private DatagramSocket server = null;
	
	public MainServer() {
		continueThread = true;
		
		threadListener = new Thread(new UDPListener());
		threadListener.start();
	}
	
	@SuppressWarnings("deprecation")
	public void stop(){
		this.continueThread = false;
		server.close();
		server = null;
		this.threadListener.stop();
		this.threadListener = null;
	}
	
	private class UDPListener implements Runnable{
		
		@Override
		public void run() {
			
			Log.addLog("ATK主线程已启动...");
			
			while(true){
				try {
					server = new DatagramSocket(Const.PORT_UDP_RECEIVE);
					break;
				} catch (SocketException e) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
				}
			}
			
			Log.addLog("开始监听本地UDP端口!端口号:"
					.concat(Integer.toString(Const.PORT_UDP_RECEIVE)));
			
			DatagramPacket dpr = null;
			while(continueThread){
				dpr = new DatagramPacket(new byte[Const.SIZE_UDP_DATA],
						Const.SIZE_UDP_DATA);
				try {
					server.receive(dpr);
					Log.addLog("获取到UDP请求数据,数据长度:"
							.concat(Integer.toString(dpr.getLength())
								.concat(",来自IP:").concat(dpr.getAddress().getHostAddress())
									));
				} catch (IOException e) {
					Log.addLog("UDP端口监听接收数据出错,错误描述"
							.concat(e.getMessage()));
				}
				if(dpr != null){
					DataHandle.handle(dpr);
				}
			}
		}
		
	}
}
